FROM php:7.2-apache

RUN a2enmod rewrite \
  && apt-get update \
  && apt-get install --yes --no-install-recommends libpq-dev zlib1g-dev\
  && docker-php-ext-install pdo_pgsql pdo_mysql zip

COPY  --from=composer:lastest /usr/bin/composer /usr/bin/composer